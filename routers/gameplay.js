const express = require("express");
const router = express.Router();
const gameplayController = require('../controllers/gameplay')
const passport = require("../lib/passport");

// POST /join
// >> untuk join bermain di room tertentu
router.post(
    "/join",
    passport.authenticate("jwt", {
      session:false
      }),
      gameplayController.joinRoom
  );

// POST /play
// >> untuk bermain
router.post(
  "/join/play",
  passport.authenticate("jwt", {
    session:false
    }),
    gameplayController.playGame
);

// GET /winner/{roomCode}
// >> untuk cek pemenang di room tertentu

module.exports = router;