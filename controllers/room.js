const { Room } = require("../models");

exports.mainPage = (req,res)=>(
    res.send("Welcome to Main Page")
)

exports.createRoom = async (req, res) => {
    const room = await Room.create(req.body)
    res.send(room)
  };