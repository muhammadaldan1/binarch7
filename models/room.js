'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Room.hasMany(models.gameplay,{
        foreignKey : "roomId",
      })
    }
  }
  Room.init({
    roomName : DataTypes.STRING,
    status:{
      type: DataTypes.ENUM,
      values: ['OPEN', 'ON_GOING', 'END'],
      allowNull:false,
      defaultValue:"OPEN"
    },
  }, {
    sequelize,
    modelName: 'Room',
  });
  return Room;
};