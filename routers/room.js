const express = require("express");
const router = express.Router();
const roomController = require('../controllers/room')
const passport = require("../lib/passport");


//Get/ Room
///menuju ke room

router.get(
    "/",
    passport.authenticate("jwt", {
      session:false
      }),
      roomController.mainPage
  );

// POST /room
// >> membuat room bermain
router.post(
  "/",
  passport.authenticate("jwt", {
    session:false
    }),
    roomController.createRoom
);

module.exports = router;