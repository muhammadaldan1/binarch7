const express = require("express");
const router = express.Router();
const userController = require('../controllers/authentication')
const passport = require("../lib/passport");


// POST /login
// >> untuk login pemain
router.post(
    "/login",
    userController.loginSuccess
  );

router.get("/login",userController.loginPage)

// POST /registration
// >> untuk registrasi pemain

router.post("/register",userController.registUser)





module.exports = router;