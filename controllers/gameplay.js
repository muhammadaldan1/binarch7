const { gameplay } = require("../models");


//------- masuk room dengan menyortir maksimal 2 orang dalam 1 room------------
exports.joinRoom = async (req, res) => {
  try {
    //-------------------------------sortir room-----------------------
    // Mencari room
    const findRoom = await gameplay.findAll({
      where: {
        roomId: req.body.roomId,
      },
    });
    

    // Jumlah room yang ditemukan
    const countRoom = findRoom.length;

    // cek validitas kode room
    if(countRoom<1){
      res.status(404).send("kode room tidak sesuai!!!")
      return
    }

    // Jika jumlah room melebihi 2, kembalikan respons error
    if (countRoom >= 2) {
      return res
        .status(400)
        .json({ error: 'Batas Pemain Sudah Mencukupi, Silahkan Membuat Room Baru!' });
    }
    //--------------------------------------------------------------------------------

    //----------------------sortir pengguna dalam 1 room ------------------------------

    // Memeriksa apakah pengguna sudah ada pada room yang sama
    const findPlayerOnSameRoom = await gameplay.findAll({
      where: {
        roomId: req.body.roomId,
        userId: req.user.id,
      },
    });

    // Jumlah pemain dalam room yang sama
    const countPlayerOnRoom = findPlayerOnSameRoom.length;

    // Jika pengguna sudah ada pada room tersebut, kembalikan respons error
    if (countPlayerOnRoom >= 1) {
      return res.status(400).json({ error: 'Anda sudah berada di ruangan' });
    }
    //--------------------------------------------------------------------------------

    // Item valid, lanjutkan dengan operasi lainnya
    await gameplay.create({
      roomId: req.body.roomId,
      userId: req.user.id,
    });

    res.send("Selamat Bermain");
  } catch (error) {
    console.log(error);
    res.status(500).send(error.message);
  }
};
//-----------------------------------------------------------------------------------



//----------------------------input game option for player--------------------------
exports.playGame = (req,res)=>{
  try {
    //-------masukan senjata pilihan user ke database-------------

    // ------------------------ create------------------------


    //-------------------------------------------------------

    //----------------------insert ke DB Room status On progress-----------------

    //---------------------------------------------------------------------------
      // database yang diperlukan
      //userId (relasi one to many), roomId (relasi one to many),roomId,
    //----------------------------------------------------------

  } catch (error) {
    console.log(error);
    res.status(500).send(error.message);
  }
}

//-------------------------------------------------------------------------------


//-----------------------------logik penentuan pemenang--------------------------


//-------------------------------------------------------------------------------
